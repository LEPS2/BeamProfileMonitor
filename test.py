#! /usr/bin/python3
#coding:utf-8
import serial
from time import sleep
import datetime
import sys
import common as cmn


def main() :
    rs1 = cmn.ConnectSerial1()
    cmn.Ini(rs1)
    sleep(10)
    cmn.GoORGX(rs1)
    cmn.GoORGY(rs1)
    sleep(5)
    cmn.GoX(rs1,21.5)
    cmn.GoY(rs1,24.5)
    sleep(5)
    cmn.AskPositionX(rs1)
    cmn.AskPositionY(rs1)

        
if __name__ == '__main__' :
    main()
