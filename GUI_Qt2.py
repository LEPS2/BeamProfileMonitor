#! /usr/bin/python3
#coding:utf-8
import sys
import random
import time
from time import sleep
import datetime
import threading
from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QVBoxLayout, QHBoxLayout, QLineEdit
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Qt5Agg')
import common as cmn
global x_pos
global y_pos
global pos_interval
global time_interval
global x_values
global y_values
global scalerX_values
global scalerY_values
global scalerXUpv_values
global scalerYUpv_values
global scalerXTag_values
global scalerYTag_values
global time_values
global tagger_values
global thread_running
global rs1
global rs2


rs1 = cmn.ConnectSerial1()
rs2 = cmn.ConnectSerial2()
cmn.MoveIni(rs1)
time.sleep(5)
x_pos = cmn.AskPositionX(rs1) 
y_pos = cmn.AskPositionY(rs1)
cmn.ScalerClear(rs2)
x_scaler = 0
y_scaler = 0
pos_interval = 1
time_interval = 1
x_values = []
y_values = []
scalerX_values = []
scalerY_values = []
scalerXUpv_values = []
scalerYUpv_values = []
scalerXTag_values = []
scalerYTag_values = []
time_values = []
tagger_values = []
thread_running = False

class MyWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setWindowTitle('Beam Profile Monitor')
        self.setGeometry(100, 100, 800, 600)

        # Widgets
        self.fig, (self.ax1, self.ax2) = plt.subplots(1, 2) 
        self.canvas = FigureCanvas(self.fig)

        self.plot_button = QPushButton('START', self)
        self.plot_button.clicked.connect(self.plotGraph)

        self.reset_button = QPushButton('RESET', self)
        self.reset_button.clicked.connect(self.reset)

        self.close_button = QPushButton('CLOSE', self)
        self.close_button.clicked.connect(self.close)

        self.time_interval_entry = QLineEdit(self)
        self.time_interval_entry.setText(str(time_interval))
        self.pos_interval_entry = QLineEdit(self)
        self.pos_interval_entry.setText(str(pos_interval))

        # Apply style sheet to change colors and styles
        self.setStyleSheet("""
            QWidget {
                background-color: #f0f0f0; /* メインウィンドウの背景色を設定 */
                color: #333; /* テキストの色を設定 */
            }
            QPushButton {
                background-color: #4CAF50; /* ボタンの背景色を設定 */
                color: white; /* ボタンのテキスト色を設定 */
                border: none; /* ボタンのボーダーを非表示にする */
                padding: 10px 20px; /* ボタンのパディングを設定 */
                border-radius: 5px; /* ボタンの角丸を設定 */
            }
            QPushButton:hover {
                background-color: #45a049; /* ボタンにマウスを重ねたときの背景色を設定 */
            }
            QLineEdit {
                background-color: #fff; /* テキストボックスの背景色を設定 */
                border: 1px solid #ccc; /* テキストボックスのボーダーを設定 */
                padding: 5px; /* テキストボックスのパディングを設定 */
                border-radius: 3px; /* テキストボックスの角丸を設定 */
            }
        """)

        # Layout
        input_layout = QHBoxLayout() 
        input_layout.addWidget(QLabel('time interval:'))
        input_layout.addWidget(self.time_interval_entry)
        input_layout.addWidget(QLabel('position interval:'))
        input_layout.addWidget(self.pos_interval_entry)

        button_layout = QHBoxLayout()
        button_layout.addWidget(self.plot_button)
        button_layout.addWidget(self.reset_button)
        button_layout.addWidget(self.close_button)

        layout = QVBoxLayout()
        layout.addWidget(self.canvas)
        layout.addLayout(input_layout)
        layout.addLayout(button_layout)

        self.setLayout(layout)

    def moveX(self):
        global x_pos
        global y_pos
        global pos_interval
        global rs1
        cmn.GoX(rs1,float(pos_interval))
        sleep(1)
        x_pos = cmn.AskPositionX(rs1)
        y_pos = cmn.AskPositionY(rs1)
        print("X Move to:", x_pos)

    def moveY(self):
        global x_pos
        global y_pos
        global pos_interval
        global rs1
        cmn.GoY(rs1,float(pos_interval))
        sleep(1)
        x_pos = cmn.AskPositionX(rs1)
        y_pos = cmn.AskPositionY(rs1)
        print("Y Move to:", y_pos)

    def getScaler(self):
        global x_values
        global y_values
        global scalerX_values
        global scalerY_values
        global scalerXUpv_values
        global scalerYUpv_values
        global scalerXTag_values
        global scalerYTag_values
        global time_values
        global tagger_values
        global x_pos
        global y_pos
        global x_scaler
        global y_scaler
        global rs2
        data = cmn.ScalerRead(rs2)
        x_values.append(x_pos)
        y_values.append(y_pos)
        scalerX_values.append(data[6])
        scalerY_values.append(data[7])
        scalerXUpv_values.append(data[9])
        scalerYUpv_values.append(data[10])
        scalerXTag_values.append(data[12])
        scalerYTag_values.append(data[13])
        time_values.append(data[5])
        tagger_values.append(data[4])
        print('Get scaler x y cntx cnty time)',x_pos,y_pos,data[4],data[5],data[8])


    def plotGraph(self):
        global thread_running
        global pos_interval
        global time_interval
        global rs1
        global rs2
        pos_interval = int(self.pos_interval_entry.text())
        time_interval = int(self.time_interval_entry.text())
        thread_running = True
        ##range : -18 - 18
        search_range = 36
        n = int(search_range / pos_interval) + 1
        
        def updateGraph():
            global thread_running
            global rs2
            global x_values
            global y_values
            global scalerX_values
            global scalerY_values
            global time_values
            global tagger_values

            while thread_running:
                print("Search points:", n)
                
                #scan X,Y
                for i in range(n):
                    if not thread_running:
                        print("Plotting stopped by user.")
                        return
                    if i == 0:
                        cmn.ScalerClear(rs2)
                        start_time = time.time()
                        cmn.ScalerBegin(rs2)
                        time.sleep(time_interval)
                        cmn.ScalerEnd(rs2)
                        end_time = time.time()
                        elapsed_time = end_time - start_time
                        print("Elapsed Time:", elapsed_time, "seconds")
                        data  = self.getScaler()
                        self.ax1.clear()
                        self.ax2.clear()
                        self.ax1.plot(x_values, scalerX_values, marker='o', linestyle='-')
                        self.ax2.plot(y_values, scalerY_values, marker='o', linestyle='-')
                        self.canvas.draw()
                    else:
                        self.moveX()
                        self.moveY()
                        cmn.ScalerClear(rs2)
                        start_time = time.time()
                        cmn.ScalerBegin(rs2)
                        time.sleep(time_interval)
                        cmn.ScalerEnd(rs2)
                        end_time = time.time()
                        elapsed_time = end_time - start_time
                        print("Elapsed Time:", elapsed_time, "seconds")
                        data = self.getScaler()
                        self.ax1.clear()
                        self.ax2.clear()
                        self.ax1.plot(x_values, scalerX_values, marker='o', linestyle='-')
                        self.ax2.plot(y_values, scalerY_values, marker='o', linestyle='-')
                        self.canvas.draw()
                
                current_datetime = datetime.datetime.now()
                formatted_datetime = current_datetime.strftime("%Y-%m-%d_%H_%m")
                filename = "./data/BPM_{}.txt".format(formatted_datetime)
                with open(filename, "w") as f:
                    f.write("x_value y_value scalerX scalerY scalerX_Upv scalreY_Upv scalerX_tag scalerY_tag time\n")
                    for i in range(len(x_values)):
                        f.write("{} {} {} {} {} {} {} {} {} {}\n".format(x_values[i], y_values[i], scalerX_values[i], scalerY_values[i], scalerXUpv_values[i], scalerYUpv_values[i], scalerXTag_values[i], scalerYTag_values[i], time_values[i], tagger_values[i]))
                        
                print("Write to Output File : ",filename)
                thread_running = False
        thread = threading.Thread(target=updateGraph)
        thread.start()
    


    def stopPlotting(self):
        global thread_running
        thread_running = False

    def reset(self):
        global x_values
        global y_values
        global scalerX_values
        global scalerY_values
        global scalerXUpv_values
        global scalerYUpv_values
        global scalerXTag_values
        global scalerYTag_values
        global time_values
        global tagger_values
        global x_pos
        global y_pos
        global time_interval
        global thread_running
        global rs1
        global rs2
        self.stopPlotting()
        time.sleep(time_interval + 1)
        cmn.MoveIni(rs1)
        x_pos = cmn.AskPositionX(rs1)
        y_pos = cmn.AskPositionY(rs1)
        cmn.ScalerClear(rs2)

        x_scaler = 0
        y_scaler = 0
        x_values = []
        y_values = []
        scalerX_values = []
        scalerY_values = []
        scalerXUpv_values = []
        scalerYUpv_values = []
        scalerXTag_values = []
        scalerYTag_values = []
        time_values = []
        tagger_values = []
        self.ax1.clear()
        self.ax2.clear()
        self.canvas.draw()
        print("Reset Done!!")

if __name__ == '__main__':
    app = QApplication(sys.argv)
    mywindow = MyWindow()
    mywindow.show()
    sys.exit(app.exec_())

