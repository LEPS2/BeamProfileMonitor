import serial
import threading

ser = serial.Serial('/dev/ttyUSB0', '9600', timeout=3)
def receive():
    while(1):
        line = ser.readline()
        if(len(line)>0):
            print(line)  
        if(line==b'test_str'):
            break  
thread = threading.Thread(target=receive)
thread.start()
ser.write(b'test_str')
thread.join()
ser.close()  
