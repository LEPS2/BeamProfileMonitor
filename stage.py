
#! /usr/bin/python3
#coding:utf-8
import PySimpleGUI as psg
import serial
from time import sleep
import datetime
import sys
import common as cmn


def main() :
    rs = cmn.ConnectSerial()
    cmn.Ini(rs)
    layout=[
        [psg.Button("-x",size=(15,1),key="BTN_BACK")],
        [psg.Button("+x",size=(15,1), key="BTN_GO")],
        [psg.Button("stop", size=(7,1), key="BTN_STOP")]
        ]

    window=psg.Window("controller", layout)
     
    while True:
        event, values = window.read()
        if event is None: break
        if event == "BTN_GO":
            cmn.GoCCW(rs)
        if event == "BTN_BACK":
            cmn.GoCW(rs)
        elif event == "BTN_STOP":
            cmn.StopX(rs)
            break

        
if __name__ == '__main__' :
    main()
