import serial
from time import sleep


usb_dev1 = '/dev/ttyUSB0'
usb_dev2 = '/dev/ttyUSB1'

# debug option 
#debug = False
debug = True

def SendCommandScaler(rs, cmd) :
    bcmd = cmd.encode('utf-8')
    c = bcmd 
    rs.write(c)
    rs.flush()
    print('send command ' + cmd)
    ans = rs.readline()
    print(ans.decode())
    return ans.decode()

def SendCommandStage(rs, cmd) :
    bcmd = cmd.encode('utf-8')
    c = bcmd + b'\r'
    rs.write(c)
    rs.flush()
    print('send command ' + cmd)
    ans = rs.readline()
    print(ans.decode())
    return ans.decode()


def ConnectSerial2() :
    rs = serial.Serial(usb_dev2)
    rs.baudrate = 9600
    rs.parity = serial.PARITY_NONE
    rs.bytesize = serial.EIGHTBITS
    rs.stopbits = serial.STOPBITS_ONE
    rs.timeout = 1
    return rs

def ScalerClear(rs) :
    a = SendCommandScaler(rs,'c')
    print('Scaler Clear : ' + a)

def ScalerBegin(rs) :
    a = SendCommandScaler(rs,'b')
    print('Scaler Begin : ' + a)
def ScalerEnd(rs) :
    a = SendCommandScaler(rs,'e')
    print('Scaler End : ' + a)

def ScalerRead(rs) :
    cmd = 'r'
    bcmd = cmd.encode('utf-8')
    c = bcmd 
    rs.write(c)
    rs.flush()
    print('send command ' + cmd)
    ans = rs.readline()
    print(ans.decode())
    ans = rs.readline()
    print(ans.decode())
    hex_str_8 = ans.decode()
    hex_list_8 = hex_str_8.split()
    ans = rs.readline()
    print(ans.decode())
    hex_str_7 = ans.decode()
    hex_list_7 = hex_str_7.split()
    decimal_values = []
    for hex_val in hex_list_8:
        decimal_val = int(hex_val, 16)
        decimal_values.append(decimal_val)

    for hex_val in hex_list_7:
        decimal_val = int(hex_val, 16)
        decimal_values.append(decimal_val)

    print(decimal_values)
    return decimal_values


def ConnectSerial1() :
    rs = serial.Serial(usb_dev1)
    rs.baudrate = 38900
    rs.parity = serial.PARITY_NONE
    rs.bytesize = serial.EIGHTBITS
    rs.stopbits = serial.STOPBITS_ONE
    rs.timeout = 1
    return rs


def Debug(rs) :
    cmd = b'*idn?\r'
    rs.write(cmd)
    print('write : ' + cmd.decode())
    rs.flush()
    line = rs.readline()
    print('read  : ' + line.decode())

def Ini(rs) :
    SendCommandStage(rs, 'axi1:F0 6000:unit PULS:MEMSW0 1:standard 1:pulse 5000:drdiv 1')
    SendCommandStage(rs, 'axi2:F0 6000:unit PULS:MEMSW0 1:standard 1:pulse 5000:drdiv 1')

def MoveIni(rs) : 
    Ini(rs)
    GoORGX(rs)
    GoORGY(rs)
    sleep(10)
    GoX(rs,20.5)
    GoY(rs,26)
    sleep(5)
    SetPosition0X(rs)
    SetPosition0Y(rs)
    BackX(rs,18)
    BackY(rs,18)
    sleep(5)

def SetPosition0X(rs) :
    SendCommandStage(rs, 'axi1:pos 0')

def SetPosition0Y(rs) :
    SendCommandStage(rs, 'axi2:pos 0')

def AskPositionX(rs) :
    s=SendCommandStage(rs, 'axi1:pos?')
    if s == '':s='0.0'
    return float(s);

def AskPositionY(rs) :
    s=SendCommandStage(rs, 'axi2:pos?')
    if s == '':s='0.0'
    return float(s);

def GoX(rs,pos) :
    pulse = int(pos*1000)
    cmd = 'axi1:pulse {}:go 1'.format(pulse)
    SendCommandStage(rs, cmd)
    
def BackX(rs,pos) :
    pulse = int(pos*1000)
    cmd = 'axi1:pulse {}:go 0'.format(pulse)
    SendCommandStage(rs, cmd)

def StopX(rs) :
    SendCommandStage(rs, 'axi1:stop 0')

def GoORGX(rs) :
    SendCommandStage(rs, 'axi1:go ORG')

def GoCCWX(rs) :
    SendCommandStage(rs, 'axi1:go CCW')

def GoCWX(rs) :
    SendCommandStage(rs, 'axi1:go CW')

def GoY(rs,pos) :
    pulse = int(pos*1000)
    cmd = 'axi2:pulse {}:go 1'.format(pulse)
    SendCommandStage(rs, cmd)
    
def BackY(rs,pos) :
    pulse = int(pos*1000)
    cmd = 'axi2:pulse {}:go 0'.format(pulse)
    SendCommandStage(rs, cmd)

def StopY(rs) : 
    SendCommandStage(rs, 'axi2:stop 0')

def GoORGY(rs) :
    SendCommandStage(rs, 'axi2:go ORG')

def GoCCWY(rs) :
    SendCommandStage(rs, 'axi2:go CCW')
def GoCWY(rs) :
    SendCommandStage(rs, 'axi2:go CW')


def SetSpeedX(rs, speed) :
    cmd_vol = 'axi1:F0 {:.2f}\r'.format(speed)
    SendCommandStage(rs, cmd_vol)

def SetSpeedY(rs, speed) :
    cmd_vol = 'axi2:F0 {:.2f}\r'.format(speed)
    SendCommandStage(rs, cmd_vol)
