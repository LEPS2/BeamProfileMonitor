#! /usr/bin/python3
#coding:utf-8
import serial
from time import sleep
import datetime
import sys
import common as cmn


def main() :
    rs1 = cmn.ConnectSerial1()
    cmn.Ini(rs1)
    cmn.GoX(rs1,0.5)
    cmn.AskPositionX(rs1)

        
if __name__ == '__main__' :
    main()
