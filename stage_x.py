
#! /usr/bin/python3
#coding:utf-8
import PySimpleGUI as psg
import serial
from time import sleep
import datetime
import sys
import common as cmn


def main() :
    rs = cmn.ConnectSerial()
    cmn.Ini(rs)
    buttons_layout1 = [
        [psg.Button('+1',key='p1',size=(5,1)),psg.Button('+2',key='p2',size=(5,1)),
            psg.Button('+10',key='p10',size=(5,1))]
        ]
    buttons_layout2 = [
        [psg.Button('-1',key='m1',size=(5,1)),psg.Button('-2',key='m2',size=(5,1)),
            psg.Button('-10',key='m10',size=(5,1))]
        ]
    output_layout = [[psg.Text('0', key='-OUTPUT-', size=(12, 1), justification='center')]]

    stop_button_layout = [
        [psg.Button('Pos?', key='pos', button_color=('white', 'blue'), size=(15, 2)),
        psg.Button('set 0', key='set_pos', button_color=('white', 'green'), size=(15, 2))]
    ]

    layout = [
        [psg.Column(buttons_layout1, justification='center')],
        [psg.Column(buttons_layout2, justification='center')],
        [psg.Column(output_layout, justification='center')],
        [psg.Column(stop_button_layout, justification='center')]
        ]

    window=psg.Window("X controller", layout)

    count = 0  # 数字の初期値

    while True:
        event, values = window.read()
        if event == psg.WINDOW_CLOSED  :
            break
        if event=='pos' :
            pos_puls = float(cmn.AskPositionX(rs))
            pos_mm = pos_puls / -1000 
            window['-OUTPUT-'].update(pos_mm)
        if event=='set_pos' : 
            cmn.SetPosition0X(rs)
            pos_puls = float(cmn.AskPositionX(rs))
            pos_mm = pos_puls / -1000 
            window['-OUTPUT-'].update(pos_mm)
        if event == "p1":
            cmn.GoX(rs,1);
            sleep(1)
            pos_puls = float(cmn.AskPositionX(rs))
            pos_mm = pos_puls / -1000 
            window['-OUTPUT-'].update(pos_mm)
        if event == "p2":
            cmn.GoX(rs,2)
            sleep(1)
            pos_puls = float(cmn.AskPositionX(rs))
            pos_mm = pos_puls / -1000 
            window['-OUTPUT-'].update(pos_mm)
        if event == "p10":
            cmn.GoX(rs, 10)
            sleep(1)
            pos_puls = float(cmn.AskPositionX(rs))
            pos_mm = pos_puls / -1000 
            window['-OUTPUT-'].update(pos_mm)
        if event == "m1":
            cmn.BackX(rs,1)
            sleep(1)
            pos_puls = float(cmn.AskPositionX(rs))
            pos_mm = pos_puls / -1000 
            window['-OUTPUT-'].update(pos_mm)
        if event == "m2":
            cmn.BackX(rs,2)
            sleep(1)
            pos_puls = float(cmn.AskPositionX(rs))
            pos_mm = pos_puls / -1000 
            window['-OUTPUT-'].update(pos_mm)
        if event == "m10":
            cmn.BackX(rs,10)
            sleep(1)
            pos_puls = float(cmn.AskPositionX(rs))
            pos_mm = pos_puls / -1000 
            window['-OUTPUT-'].update(pos_mm)

    window.close()

        
if __name__ == '__main__' :
    main()
