#! /usr/bin/python3
#coding:utf-8
import serial
from time import sleep
import datetime
import sys
import common as cmn


def main() :
    rs = cmn.ConnectSerial()
    cmn.AskPosition(rs)
    cmn.AskUnit(rs)
    cmn.GoX(rs)

        
if __name__ == '__main__' :
    main()
